declare var angular: ng.IAngularStatic;

module myApp {
  interface IUppercaseUserEntity {
    id: string,
    Address: string,
    Name: string,
    Note: string,
    Number: string,
    Phone: string,
    isEditing?: boolean
  }

  export class myService {
    static $inject = ['$http', '$q'];

    constructor(private $http: ng.IHttpService,
                private $q: ng.IQService
    ) {
    }

    public getTableFields () {
      let defer = this.$q.defer();

      this.$http({
        method: 'GET',
        url: 'http://localhost:3000/Fields'
      })
      .then(response => {
        defer.resolve(response.data);
      })
      .catch(rejection => {
        defer.reject(rejection.data);
      });

      return defer.promise;
    }

    public getTableData () {
      let defer = this.$q.defer();

      this.$http({
        method: 'GET',
        url: 'http://localhost:3000/Data'
      })
      .then(response => {
        defer.resolve(response.data);
      })
      .catch(rejection => {
        defer.reject(rejection.data);
      });

      return defer.promise;
    }

    public addTableData (requestData: IUppercaseUserEntity) {
      let defer = this.$q.defer();

      this.$http({
        method: 'POST',
        url: 'http://localhost:3000/Data',
        data: requestData
      })
      .then(response => {
        defer.resolve(response.data);
      })
      .catch(rejection => {
        defer.reject(rejection.data);
      });

      return defer.promise;
    }

    public deleteTableData (id: string) {
      let defer = this.$q.defer();

      this.$http({
        method: 'DELETE',
        url: `http://localhost:3000/Data/${ id }`
      })
      .then(response => {
        defer.resolve(response.data);
      })
      .catch(rejection => {
        defer.reject(rejection.data);
      });

      return defer.promise;
    }

    public updateTableData (id: string, requestData: IUppercaseUserEntity) {
      let defer = this.$q.defer();

      this.$http({
        method: 'PUT',
        url: `http://localhost:3000/Data/${ id }`,
        data: requestData
      })
      .then(response => {
        defer.resolve(response.data);
      })
      .catch(rejection => {
        defer.reject(rejection.data);
      });

      return defer.promise;
    }
  }

  angular.module('myApp').service('myService', myService);
}
