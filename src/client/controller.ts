declare var angular: ng.IAngularStatic;

module myApp {
  interface ITableTitleEntity {
    key: string,
    name: string
  }

  interface ITableDataEntity {
    [propName: string]: ILowercaseUserEntity
  }

  interface ILowercaseUserEntity {
    id: string,
    address: string,
    name: string,
    note: string,
    number: string,
    phone: string,
    isEditing?: boolean
  }

  interface IUppercaseUserEntity {
    id: string,
    Address: string,
    Name: string,
    Note: string,
    Number: string,
    Phone: string,
    isEditing?: boolean
  }

  import myService = myApp.myService;

  export class myCtrl {
    static $inject = ['$scope', '$q', '$http', '$window', 'myService'];

    public tableTitle: ITableTitleEntity[];
    public tableData: ITableDataEntity;
    public formFields: IUppercaseUserEntity;
    public temporaryOriginData: ILowercaseUserEntity;

    constructor(private $scope: ng.IScope,
                private $q: ng.IQService,
                private $http: ng.IHttpService,
                private $window: ng.IWindowService,
                private myService: myApp.myService
    ) {
      this.tableTitle = [];
      this.tableData = {};
      this.formFields = {
        id: null,
        Address: null,
        Name: null,
        Note: null,
        Number: null,
        Phone: null,
        isEditing: null
      };
      this.temporaryOriginData = {
        id: null,
        address: null,
        name: null,
        note: null,
        number: null,
        phone: null,
        isEditing: null
      };

      this.getTableTitleAndData();
    }

    public getTableTitleAndData () {
      this.$q.all([this.myService.getTableFields(), this.myService.getTableData()])
      .then(response => {
        this.tableTitle = response[0];
        this.tableTitle.push({key: 'feature', name: '功能'});

        let result: ITableDataEntity = {};

        response[1].forEach(item => {
          result[item.id] = {
            id: item.id,
            number: item.Number,
            name: item.Name,
            phone: item.Phone,
            address: item.Address,
            note: item.Note,
            isEditing: false
          }
        })

        this.tableData = result;
      })
      .catch(() => {
        console.log('Show error message');
      });
    }

    public submit () {
      let timestamp: string = String(Date.now());

      this.formFields.id = timestamp;
      this.formFields.Number = 'A003';
      this.formFields.isEditing = false;

      this.myService.addTableData(this.formFields)
      .then(() => {
        this.tableData[timestamp] = {
          id: this.formFields.id,
          number: this.formFields.Number,
          name: this.formFields.Name,
          phone: this.formFields.Phone,
          address: this.formFields.Address,
          note: this.formFields.Note,
          isEditing: this.formFields.isEditing,
        };
      })
      .catch(() => {
        console.log('Show error message');
      });
    }

    public editMember (id: string) {
      this.tableData[id].isEditing = true;

      this.temporaryOriginData[id] = {
        id: this.tableData[id].id,
        number: this.tableData[id].number,
        name: this.tableData[id].name,
        phone: this.tableData[id].phone,
        address: this.tableData[id].address,
        note: this.tableData[id].note,
        isEditing: this.tableData[id].isEditing
      };
    }

    public deleteMember (id: string) {
      this.myService.deleteTableData(id)
      .then(() => {
        delete this.tableData[id];
      })
      .catch(() => {
        console.log('Show error message');
      });
    }

    public cancelEdit (id: string) {
      this.tableData[id] = { ...this.temporaryOriginData[id] };
      this.tableData[id].isEditing = false;
      delete this.temporaryOriginData[id];
    }

    public saveEdit (id: string) {
      let requestData: IUppercaseUserEntity = {
        id: this.tableData[id].id,
        Number: this.tableData[id].number,
        Name: this.tableData[id].name,
        Phone: this.tableData[id].phone,
        Address: this.tableData[id].address,
        Note: this.tableData[id].note
      };

      this.myService.updateTableData(id, requestData)
      .then(() => {
        this.tableData[id].isEditing = false;
        delete this.temporaryOriginData[id];
        this.$window.alert('更新成功');
      })
      .catch(() => {
        console.log('Show error message');
      });
    }
  }

  angular.module('myApp').controller('myCtrl', myCtrl);
}
