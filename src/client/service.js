var myApp;
(function (myApp) {
    var myService = /** @class */ (function () {
        function myService($http, $q) {
            this.$http = $http;
            this.$q = $q;
        }
        myService.prototype.getTableFields = function () {
            // get http://localhost:3000/Fields
        };
        myService.prototype.getTableData = function () {
            // get http://localhost:3000/Data
        };
        myService.prototype.deleteTableData = function () {
            // delete http://localhost:3000/Data/{id}
        };
        myService.prototype.addTableData = function () {
            // post http://localhost:3000/Data
        };
        myService.prototype.updateTableData = function () {
            // put http://localhost:3000/Data/{id}
        };
        myService.$inject = ['$http', '$q'];
        return myService;
    }());
    myApp.myService = myService;
    angular.module("myApp")
        .service("myService", myService);
})(myApp || (myApp = {}));
//# sourceMappingURL=service.js.map