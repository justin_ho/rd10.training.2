var myAPP;
(function (myAPP) {
    angular.module("myApp", []);
    // export class myCtrl {
    //     static $inject = ['$scope', '$q', 'myService'];
    //
    //
    //     constructor(private $scope: any,
    //                 private $q: ng.IQService,
    //                 private myService: any
    //     ) {
    //     }
    //
    // }
    angular.module("myApp").controller("myCtrl", ['$scope', '$http', '$q', function ($scope, $http, $q) {
            $scope.tableTitle = [];
            $scope.tableData = [];
            function getFields() {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:3000/Fields'
                });
            }
            function getData() {
                return $http({
                    method: 'GET',
                    url: 'http://localhost:3000/Data'
                });
            }
            function getTabletitleAndData() {
                $q.all([getFields(), getData()]).then(function (res) {
                    $scope.tableTitle = res[0].data;
                    $scope.tableTitle.push({ key: 'feature', name: '功能' });
                    $scope.tableData = $scope.tableData.concat(res[1].data);
                });
            }
            getTabletitleAndData();
        }]);
})(myAPP || (myAPP = {}));
//# sourceMappingURL=app.js.map