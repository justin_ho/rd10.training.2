/// <reference path="../tsDeclarationFile/angular.d.ts" />

declare var angular: ng.IAngularStatic;

module myApp {
  angular.module('myApp', []);
}
